package bops

// Not takes a slice of bytes in big-endian representation and
// performs a not-operation for each byte. The result is returned as
// a new slice of bytes with the same length.
func Not(a []byte) []byte {
	n := len(a)
	r := make([]byte, n)

	for i := 0; i < n; i++ {
		r[i] = ^a[i]
	}

	return r
}

// And takes two slices of bytes in big-endian representation and
// performs an and-operation for each pair of bytes. The result is
// returned as a new slice of bytes with the same length as the
// longer of the given slices.
func And(a, b []byte) []byte {
	var r []byte
	var n int

	a, b, r, n = prep(a, b)

	for i := 0; i < n; i++ {
		r[i] = a[i] & b[i]
	}

	return r
}

// Nand takes two slices of bytes in big-endian representation and
// performs a nand-operation for each pair of bytes. The result is
// returned as a new slice of bytes with the same length as the
// longer of the given slices.
func Nand(a, b []byte) []byte {
	var r []byte
	var n int

	a, b, r, n = prep(a, b)

	for i := 0; i < n; i++ {
		r[i] = ^(a[i] & b[i])
	}

	return r
}

// Or takes two slices of bytes in big-endian representation and
// performs an or-operation for each pair of bytes. The result is
// returned as a new slice of bytes with the same length as the
// longer of the given slices.
func Or(a, b []byte) []byte {
	var r []byte
	var n int

	a, b, r, n = prep(a, b)

	for i := 0; i < n; i++ {
		r[i] = a[i] | b[i]
	}

	return r
}

// Nor takes two slices of bytes in big-endian representation and
// performs a nor-operation for each pair of bytes. The result is
// returned as a new slice of bytes with the same length as the
// longer of the given slices.
func Nor(a, b []byte) []byte {
	var r []byte
	var n int

	a, b, r, n = prep(a, b)

	for i := 0; i < n; i++ {
		r[i] = ^(a[i] | b[i])
	}

	return r
}

// Xor takes two slices of bytes in big-endian representation and
// performs an xor-operation for each pair of bytes. The result is
// returned as a new slice of bytes with the same length as the
// longer of the given slices.
func Xor(a, b []byte) []byte {
	var r []byte
	var n int

	a, b, r, n = prep(a, b)

	for i := 0; i < n; i++ {
		r[i] = a[i] ^ b[i]
	}

	return r
}

// Prep takes two slices of bytes in big-endian representation and
// prepends zero-bytes to the shorter of the given slices to match
// the length of the longer slice. Returns the modified slices, a
// new slice of zero-bytes with the same length as those and the
// length itself.
func prep(a, b []byte) ([]byte, []byte, []byte, int) {
	if len_a, len_b := len(a), len(b); len_a < len_b {
		t := make([]byte, len_b)
		copy(t[len_b-len_a:], a)
		return t, b, make([]byte, len_b), len_b
	} else {
		t := make([]byte, len_a)
		copy(t[len_a-len_b:], b)
		return a, t, make([]byte, len_a), len_a
	}
}
