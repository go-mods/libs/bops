package bops

import (
	"math"
	"math/big"
)

// ShiftL takes a slice of bytes in big-endian representation and
// shifts the bits of each byte n positions left. Bits that would be
// discarded this way will be added to the previous byte
// accordingly. Returns the result as a new slice of bytes with the
// same length as a.
func ShiftL(a []byte, n int) []byte {
	x := big.NewInt(0)
	x.SetBytes(a)
	x = x.Mul(x, big.NewInt(int64(math.Pow(2, float64(n)))))
	r := x.Bytes()

	if l, j := len(r), len(a); l < j {
		r = append(make([]byte, j-l), r...)
		return r
	}

	return r[len(r)-len(a):] // discard left bytes
}

// ShiftR takes a slice of bytes in big-endian representation and
// shifts the bits of each byte n positions right. Bits that would
// be discarded this way will be added to the next byte accordingly.
// Returns the result as a new slice of bytes with the same length
// as a.
func ShiftR(a []byte, n int) []byte {
	x := big.NewInt(0)
	x.SetBytes(a)
	x = x.Div(x, big.NewInt(int64(math.Pow(2, float64(n)))))
	r := x.Bytes()
	j := len(a)

	if l := len(r); l < j {
		r = append(make([]byte, j-l), r...)
	}

	return r[:j] // discard right bytes
}

// Add takes two slices of bytes in big-endian representation and
// adds b to a. The result is returned as a new slice of bytes with
// the same length as the longer of the given slices.
func Add(a, b []byte) []byte {
	n := max(len(a), len(b))
	x := big.NewInt(0)
	y := big.NewInt(0)
	x.SetBytes(a)
	y.SetBytes(b)
	r := x.Add(x, y).Bytes()

	if l := len(r); l < n {
		r = append(make([]byte, n-l), r...)
		return r
	}

	return r[len(r)-n:] // discard left bytes
}

// Sub takes two slices of bytes in big-endian representation and
// subtracts b from a. The result is returned as a new slice of
// bytes with the same length as the longer of the given slices.
func Sub(a, b []byte) []byte {
	n := max(len(a), len(b))
	x := big.NewInt(0)
	y := big.NewInt(0)
	x.SetBytes(a)
	y.SetBytes(b)
	r := x.Sub(x, y).Bytes()

	if l := len(r); l < n {
		r = append(make([]byte, n-l), r...)
	}

	return r
}

// Mul takes two slices of bytes in big-endian representation and
// multiplies a with b. The result is returned as a new slice of
// bytes with the same length as the longer of the given slices.
func Mul(a, b []byte) []byte {
	n := max(len(a), len(b))
	x := big.NewInt(0)
	y := big.NewInt(0)
	x.SetBytes(a)
	y.SetBytes(b)
	r := x.Mul(x, y).Bytes()
	l := len(r)

	if l < n {
		r = append(make([]byte, n-l), r...)
		return r
	}

	return r[l-n:] // discard left bytes
}

// Div takes two slices of bytes in big-endian representation and
// divides a by b. The result is returned as a new slice of bytes
// with the same length as the longer of the given slices.
func Div(a, b []byte) []byte {
	n := max(len(a), len(b))
	x := big.NewInt(0)
	y := big.NewInt(0)
	x.SetBytes(a)
	y.SetBytes(b)
	r := x.Div(x, y).Bytes()

	if l := len(r); l < n {
		r = append(make([]byte, n-l), r...)
	}

	return r
}

// Max takes a sequence of integers and returns the max value or
// math.MinInt if no value was provided.
func max(n ...int) int {
	m := math.MinInt

	for _, z := range n {
		if z > m {
			m = z
		}
	}

	return m
}
